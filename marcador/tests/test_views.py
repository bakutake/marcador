from django.contrib.auth.models import User, AnonymousUser
from django.test import Client, TestCase
from django.core.exceptions import PermissionDenied
# import pytests
import unittest

from ..views import bookmark_list, bookmark_user, bookmark_create, bookmark_edit
from ..models import Bookmark, Tag


class BookmarkListTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.admin = User.objects.create_superuser(
            username='admin',
            email='admin@admin.com',
            password='password',
        )
        self.anonym = AnonymousUser()
        self.user = User.objects.create_user(
            username='user',
            email='mail@mail.com',
            password='password',
        )

    def test_bookmark_list_all(self):
        bookmark = Bookmark(
            url='https://google.com/',
            title='title_bookmark_example',
            description='title bookmark example',
            owner=self.admin,
        )
        bookmark.save()
        bookmark = Bookmark(is_public=True, url='site.com/', title='title',
                            description='description', owner=self.admin)
        bookmark.save()
        response = self.client.get('/')
        bookmarks = Bookmark.public.all()
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['bookmarks'],
                                 map(repr, bookmarks))

    def test_bookmark_list_empty_list(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['bookmarks']), 0)

    def test_bookmark_list_with_tag_only(self):
        # bookmark without tag
        bookmark = Bookmark(url='site.com/', title='title',
                            description='description', owner=self.admin)
        bookmark.save()
        # bookmark with tag
        bookmark = Bookmark(url='site.com/', title='title',
                            description='description', owner=self.admin)
        bookmark.save()
        tag = Tag(name='example_tag_name')
        tag.save()
        bookmark.tags.add(tag)

        bookmarks = Bookmark.public.all().filter(tags__name='example_tag_name')
        response = self.client.get('/', data={'tag': 'example_tag_name'})

        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['bookmarks'],
                                 map(repr, bookmarks))
        self.assertEqual(len(response.context['bookmarks']), 1)

    def test_bookmark_list_without_private(self):
        # publbic bookmark
        bookmark = Bookmark(
            url='https://google.com/',
            title='title_bookmark_example',
            description='title bookmark example',
            owner=self.admin,
        )
        bookmark.save()
        # private bookmark
        bookmark = Bookmark(is_public=False, url='site.com/', title='title',
                            description='description', owner=self.admin)
        bookmark.save()
        response = self.client.get('/')
        bookmarks = Bookmark.public.all()
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['bookmarks'],
                                 map(repr, bookmarks))
        self.assertEqual(len(response.context['bookmarks']), 1)


class BookmarkUserTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.admin = User.objects.create_superuser(
            username='admin',
            email='admin@admin.com',
            password='password',
        )
        self.anonym = AnonymousUser()
        self.user = User.objects.create_user(
            username='user',
            email='mail@mail.com',
            password='password',
        )

    def test_bookmark_user_not_logined(self):
        # public user bookmark
        bookmark = Bookmark(is_public=True, url='site.com/', title='public',
                            description='description', owner=self.user)
        bookmark.save()
        # private user bookmark
        bookmark = Bookmark(is_public=False, url='site.com/', title='private_user',
                            description='description', owner=self.user)
        bookmark.save()
        # private admin bookmark
        bookmark = Bookmark(is_public=False, url='site.com/', title='private_admin',
                            description='description', owner=self.admin)
        bookmark.save()

        response = self.client.get('/user/user/')
        bookmarks = Bookmark.public.filter(owner__username='user')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['bookmarks']), 1)
        self.assertQuerysetEqual(response.context['bookmarks'],
                                 map(repr, bookmarks))

    def test_bookmark_user_is_current_user(self):
        # public user bookmark
        bookmark = Bookmark(is_public=True, url='site.com/', title='public',
                            description='description', owner=self.user)
        bookmark.save()
        # private user bookmark
        bookmark = Bookmark(is_public=False, url='site.com/', title='private_user',
                            description='description', owner=self.user)
        bookmark.save()
        # private admin bookmark
        bookmark = Bookmark(is_public=False, url='site.com/', title='private_admin',
                            description='description', owner=self.admin)
        bookmark.save()

        self.client.login(username='user', password='password')

        response = self.client.get('/user/user/')
        bookmarks = self.user.bookmarks.all()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['bookmarks']), 2)
        self.assertQuerysetEqual(response.context['bookmarks'],
                                 map(repr, bookmarks))

    def test_bookmark_user_is_not_current_user(self):
        # public user bookmark
        bookmark = Bookmark(is_public=True, url='site.com/', title='public',
                            description='description', owner=self.user)
        bookmark.save()
        # private user bookmark
        bookmark = Bookmark(is_public=False, url='site.com/', title='private_user',
                            description='description', owner=self.user)
        bookmark.save()
        # private admin bookmark
        bookmark = Bookmark(is_public=False, url='site.com/', title='private_admin',
                            description='description', owner=self.admin)
        bookmark.save()

        self.client.login(username='user', password='password')

        response = self.client.get('/user/admin/')
        bookmarks = Bookmark.public.filter(owner__username='admin')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['bookmarks']), 0)
        self.assertQuerysetEqual(response.context['bookmarks'],
                                 map(repr, bookmarks))

    def test_bookmark_user_with_tag(self):
        # bookmark without tag
        bookmark = Bookmark(url='site.com/', title='title',
                            description='description', owner=self.admin)
        bookmark.save()
        # bookmark with tag
        bookmark = Bookmark(url='site.com/', title='title',
                            description='description', owner=self.admin)
        bookmark.save()
        tag = Tag(name='example_tag_name')
        tag.save()
        bookmark.tags.add(tag)

        self.client.login(username='user', password='password')

        response = self.client.get('/user/admin/',
                                   data={'tag': 'example_tag_name'})
        bookmarks = Bookmark.public.filter(tags__name='example_tag_name')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['bookmarks']), 1)
        self.assertQuerysetEqual(response.context['bookmarks'],
                                 map(repr, bookmarks))


class BookmarkCreateTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.admin = User.objects.create_superuser(
            username='admin',
            email='admin@admin.com',
            password='password',
        )
        self.anonym = AnonymousUser()
        self.user = User.objects.create_user(
            username='user',
            email='mail@mail.com',
            password='password',
        )
        # self._create_bookmark(owner=self.user, is_public=True)
        # self._create_bookmark(owner=self.admin, is_public=True)
        # self._create_bookmark(owner=self.user, is_public=False)
        # self._create_bookmark(owner=self.admin, is_public=False)
        Bookmark(is_public=True, url='site.com/', title='example',
                 description='description', owner=self.user).save()
        Bookmark(is_public=False, url='site.com/', title='example',
                 description='description', owner=self.user).save()
        Bookmark(is_public=True, url='site.com/', title='example',
                 description='description', owner=self.admin).save()
        Bookmark(is_public=False, url='site.com/', title='example',
                 description='description', owner=self.admin).save()

    # def _create_bookmark(self, owner, is_public=True, title='example'):
    #     bookmark = Bookmark(is_public=is_public, url='site.com/', title=title,
    #                         description='description', owner=owner)
    #     bookmark.save()
    #     return bookmark

    def test_create_bookmark_get(self):
        self.client.login(username="admin", password="password")
        response = self.client.get('/create/')
        self.assertEqual(response.status_code, 200)


    def test_create_bookmark_get_if_logined(self):
        self.client.login(username="admin", password="password")
        response = self.client.get('/create/')
        self.assertEqual(response.status_code, 200)

    def test_create_bookmark_get_if_not_logined(self):
        response = self.client.get('/create/')
        self.assertEqual(response.status_code, 302)

    def test_create_bookmark_post_if_logined(self):
        count_bookmarks_before = len(Bookmark.objects.all())
        self.client.login(username="admin", password="password")
        response = self.client.post(path='/create/', data={
            'url': 'http://testurl.com',
            'title':'test_title',
        })
        count_bookmarks_after = len(Bookmark.objects.all())
        last_created = Bookmark.objects.order_by().last()
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/user/admin/')
        self.assertEqual(count_bookmarks_before + 1, count_bookmarks_after)
        self.assertEqual(last_created.url, 'http://testurl.com')
        self.assertEqual(last_created.title, 'test_title')


class BookmarkEditTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.admin = User.objects.create_superuser(
            username='admin',
            email='admin@admin.com',
            password='password',
        )
        self.anonym = AnonymousUser()
        self.user = User.objects.create_user(
            username='user',
            email='mail@mail.com',
            password='password',
        )
        # self._create_bookmark(owner=self.user, is_public=True)
        # self._create_bookmark(owner=self.admin, is_public=True)
        # self._create_bookmark(owner=self.user, is_public=False)
        # self._create_bookmark(owner=self.admin, is_public=False)
        Bookmark(is_public=True, url='site.com/', title='example',
                 description='description', owner=self.user).save()
        Bookmark(is_public=False, url='site.com/', title='example',
                 description='description', owner=self.user).save()
        Bookmark(is_public=True, url='site.com/', title='example',
                 description='description', owner=self.admin).save()
        Bookmark(is_public=False, url='site.com/', title='example',
                 description='description', owner=self.admin).save()
        self.bookmark_user_example = Bookmark(is_public=True, owner=self.user,
                                              title='example', url='example.com/',
                                              description='description')
        self.bookmark_user_example.save()
        self.bookmark_admin_example = Bookmark(is_public=True, owner=self.admin,
                                               title='example', url='example.com/',
                                               description='description')
        self.bookmark_admin_example.save()

    # def _create_bookmark(self, owner, is_public=True, title='example'):
    #     bookmark = Bookmark(is_public=is_public, url='site.com/', title=title,
    #                         description='description', owner=owner)
    #     bookmark.save()
    #     return bookmark

    def test_edit_bookmark_get_if_user_is_owner(self):
        self.client.login(username="admin", password="password")
        admin_bookmark_pk = str(self.bookmark_admin_example.pk)
        response = self.client.get('/edit/' + admin_bookmark_pk)
        self.assertEqual(response.status_code, 200)

    def test_edit_bookmark_get_if_user_is_not_owner_is_superuser(self):
        self.client.login(username="admin", password="password")
        user_bookmark_pk = str(self.bookmark_admin_example.pk)
        response = self.client.get('/edit/' + user_bookmark_pk)
        self.assertEqual(response.status_code, 200)

    def test_edit_bookmark_get_if_user_is_not_owner_is_not_superuser(self):
        self.client.login(username="user", password="password")
        admin_bookmark_pk = str(self.bookmark_admin_example.pk)
        response = self.client.get('/edit/' + admin_bookmark_pk)
        self.assertEqual(response.status_code, 403)

    def test_edit_bookmark_get_if_not_logined(self):
        response = self.client.get('/edit/')
        user_bookmark_pk = str(self.bookmark_admin_example.pk)
        response = self.client.get('/edit/' + user_bookmark_pk)
        self.assertEqual(response.status_code, 302)

    def test_edit_bookmark_post_if_user_is_owner(self):
        count_bookmarks_before = len(Bookmark.objects.all())
        self.client.login(username="user", password="password")
        bookmark = self.bookmark_user_example
        response = self.client.post(path='/edit/' + str(bookmark.pk), data={
            'url': 'http://editedurl.com',
            'title':'edited',
            'description':'edited',
        })
        count_bookmarks_after = len(Bookmark.objects.all())
        bookmark_after = Bookmark.objects.get(pk=self.bookmark_user_example.pk)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/user/user/')
        self.assertEqual(count_bookmarks_before, count_bookmarks_after)
        self.assertEqual(bookmark_after.url, 'http://editedurl.com')
        self.assertEqual(bookmark_after.title, 'edited')
        self.assertEqual(bookmark_after.description, 'edited')
        self.assertFalse(bookmark_after.is_public)

    def test_edit_bookmark_post_if_user_is_superuser_is_not_owner(self):
        count_bookmarks_before = len(Bookmark.objects.all())
        self.client.login(username="admin", password="password")
        bookmark = self.bookmark_user_example
        response = self.client.post(path='/edit/' + str(bookmark.pk), data={
            'url': 'http://editedurl.com',
            'title':'edited',
            'description':'edited',
        })
        count_bookmarks_after = len(Bookmark.objects.all())
        bookmark_after = Bookmark.objects.get(pk=self.bookmark_user_example.pk)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/user/admin/')
        self.assertEqual(count_bookmarks_before, count_bookmarks_after)
        self.assertEqual(bookmark_after.url, 'http://editedurl.com')
        self.assertEqual(bookmark_after.title, 'edited')
        self.assertEqual(bookmark_after.description, 'edited')
        self.assertFalse(bookmark_after.is_public)

    def test_edit_bookmark_post_if_user_is_not_owner_is_not_superuser(self):
        count_bookmarks_before = len(Bookmark.objects.all())
        self.client.login(username="user", password="password")
        bookmark = self.bookmark_admin_example
        response = self.client.post(path='/edit/' + str(bookmark.pk), data={
            'url': 'http://editedurl.com',
            'title':'edited',
            'description':'edited',
        })
        count_bookmarks_after = len(Bookmark.objects.all())
        bookmark_after = Bookmark.objects.get(pk=self.bookmark_admin_example.pk)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(count_bookmarks_before, count_bookmarks_after)
        self.assertEqual(bookmark_after.url, 'example.com/')
        self.assertEqual(bookmark_after.title, 'example')
        self.assertEqual(bookmark_after.description, 'description')
        self.assertTrue(bookmark_after.is_public)


    def test_edit_bookmark_post_change_owner(self):
        # data changed, but user is not changed
        count_bookmarks_before = len(Bookmark.objects.all())
        self.client.login(username="admin", password="password")
        bookmark = self.bookmark_user_example
        response = self.client.post(path='/edit/' + str(bookmark.pk), data={
            'url': 'http://editedurl.com',
            'title':'edited',
            'description':'edited',
            'owner': self.admin,
        })
        count_bookmarks_after = len(Bookmark.objects.all())
        bookmark_after = Bookmark.objects.get(pk=self.bookmark_user_example.pk)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/user/admin/')
        self.assertEqual(count_bookmarks_before, count_bookmarks_after)
        self.assertEqual(bookmark_after.url, 'http://editedurl.com')
        self.assertEqual(bookmark_after.title, 'edited')
        self.assertEqual(bookmark_after.description, 'edited')
        self.assertEqual(bookmark_after.owner, self.user)
        self.assertFalse(bookmark_after.is_public)
