
The project is based on the django-marcador tutorial.
url: http://django-marcador.keimlink.de/en/
I made some changes in the project, and update some code.
So now it's work with python3.7 and django 2.2.
As well as I added unit-tests (I used coverage)

Tutorial authos:
Markus Zapke-Gründemann
Andreas Hug
Dave Brotherstone
Dinu Gherman
Ernesto Rico-Schmidt
Florian Apolloner
Markus Holtermann
Max Brauer
Raphael Ackermann
Sean Hammond
